#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pathlib
import sys

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))
print(sys.path)

import unittest

# ROS libraries
import roslib
import rospy
from time import sleep

# ROS Messages
from sensor_msgs.msg import CompressedImage
from wp4_msgs.msg import DepthFrame


class TestDepthEstimation(unittest.TestCase):

    depth_estimation_ok = False

    def _callback(self, data):
        self.depth_estimation_ok = True

    def test_if_depth_estimation_publish(self):
        rospy.Subscriber("/vision_msgs/depth_estimation", DepthFrame, self._callback)

        counter = 0
        while not rospy.is_shutdown() and counter < 5 and (not self.depth_estimation_ok):
            sleep(1)
            counter += 1

        self.assertTrue(self.depth_estimation_ok)
        
    
if __name__ == "__main__":
    import rostest
    rospy.init_node("test_depth_estimation", log_level=rospy.INFO)
    rostest.rosrun("depth-estimation", 'unittest_depth_estimation', TestDepthEstimation)