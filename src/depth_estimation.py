#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pathlib
import sys

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))
print(sys.path)

import cv2
import message_filters
import numpy as np
import rospy
import torch
from sensor_msgs.msg import CompressedImage
from wp4_msgs.msg import DepthFrame


class DepthEstimation:
    def __init__(self):
        print("Starting initialization")

        # Model
        self.midas = torch.hub.load("intel-isl/MiDaS", "MiDaS_small").cuda().eval()
        self.transformer = torch.hub.load("intel-isl/MiDaS", "transforms").small_transform

        # Published topics
        self.depth_estimation_publisher = rospy.Publisher(
            "/vision_msgs/depth_estimation",
            DepthFrame,
            queue_size=10,
        )

        # Subscribed topics
        self.head_front_image_subscriber = message_filters.Subscriber(
            rospy.get_param("~input_topic"), CompressedImage
        )
        self.head_front_image_subscriber.registerCallback(self._callback)

        print("Initialization completed")

    def _callback(self, ros_image):
        frame_original = cv2.imdecode(
            np.frombuffer(ros_image.data, np.uint8), cv2.IMREAD_COLOR
        )  # OpenCV >= 3.0:

        with torch.no_grad():
            input_batch = self.transformer(frame_original).cuda()
            prediction = self.midas(input_batch)

            prediction = torch.nn.functional.interpolate(
                prediction.unsqueeze(1),
                size=frame_original.shape[:2],
                mode="bicubic",
                align_corners=False,
            ).squeeze()

        msg = DepthFrame()
        msg.header.stamp = ros_image.header.stamp
        msg.data = prediction.flatten().cpu().tolist()
        msg.height = prediction.shape[0]
        msg.width = prediction.shape[1]

        self.depth_estimation_publisher.publish(msg)


if __name__ == "__main__":
    rospy.init_node("wp4_depth_estimation", anonymous=False)
    depth_estimation = DepthEstimation()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Closing WP4 monocular depthe estimator")
