# Depth Estimation



## Docker

### Build

```sh
docker build --target compiler --tag wp4_depth_estimation:TAG .
```

### Run

```sh
# Default run
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_depth_estimation:TAG

# Run with basestation
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_depth_estimation:TAG basestation
```

## ROS Topics

### Dependencies

 - Head front camera RGB images (from ARI or basestation)

### Published

- /vision_msgs/depth_estimation
